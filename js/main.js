$(function () {

	$(window).scroll(function () {
	  if ($(document).scrollTop() > 2) {
	      $('nav.navbar').addClass('has-shadow');
	  } else {
	      $('nav.navbar').removeClass('has-shadow');
	  }
	});

	$('.status-box textarea').on('focusout', function () {
	    //$('.status-box').removeClass('focused');
	}).on('focusin', function () {
	    $('.status-box').addClass('focused');
	});



	// $('.addTweetBtn').click(function (e) {
	// 	e.preventDefault();
	// 	$('.popup-tweet-wrap').addClass('active');
	// });

	// $('.closeTweetPopup').click(function () {
	// 	$('.popup-tweet-wrap').removeClass('active');
	// });

	$('.makeItpop').click(function (e) {
		e.preventDefault();
		$('html').addClass('is-clipped');
		$('.popup-wrap-outer').removeClass('active');
		var t = $(this).data('popup-target');
		t= '.'+t;
		$(t).addClass('active');
		console.log(t);
	});

	$('.closePopupBtn').click(function () {
		$('html').removeClass('is-clipped');
		var par = $(this).parents(".popup-wrap-outer");
		par.removeClass('active');
	});

	$('.wrap').click(function (e) {
		if( $(e.target).is('.wrap')){
			$('html').removeClass('is-clipped');
			var par = $(this).parents(".popup-wrap-outer");
			par.removeClass('active');
		}
	});

	$('.location').click(function () {
		$('.at').addClass("active");
		$('.linkField').removeClass("active");
	});

	$('.addlink').click(function () {
		$('.at').removeClass("active");
		$('.linkField').addClass("active");
	});

	$('.at input').keydown(function () {
		if($(this).val().length > 0){
			$(".at .tg-search-results").addClass("active");
		}
		else{
			$(".at .tg-search-results").removeClass("active");
		}
		
	});
	$('.at input').focusout(function () {		
		$(".at .tg-search-results").removeClass("active");
	});
	$('.rating input').change(function () {
	  var $radio = $(this);
	  $('.rating .selected').removeClass('selected');
	  $radio.closest('label').addClass('selected');
	});
	
});